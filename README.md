digital_ocean_droplet
=========

This role creates droplets on Digital Ocean.
In this directory you can also find Dynamic Ansible inventory called `digitalocean.yml`

If you want to destroy droplets you can simply change `digital_ocean_droplet_state` from `present` to `absent`

Requirements
------------

[community.digitalocean collection](https://docs.ansible.com/ansible/latest/collections/community/digitalocean/index.html)

NOTE! Environment variable `DO_API_TOKEN` must be set to grant access to Digital Ocean API.

Role Variables
--------------

defaults/main.yml:
```sh
digital_ocean_droplet_project_name (string)
digital_ocean_droplet_ssh_fingerprint (string)
digital_ocean_droplet_size (string)
digital_ocean_droplet_region (string)
digital_ocean_droplet_image (string)
digital_ocean_droplet_state (string)
digital_ocean_droplet_machines (list of machines)
```
Parameters for each machine:
```sh
type (string)
index (number)
tags (list)
```

Dependencies
------------

community.digitalocean collection

Example Playbook
----------------

Set environment variable `DO_API_TOKEN` to grant access to Digital Ocean API.

First of all you need to create droplets on Digital Ocean. Below is the example of playbook to create required droplets.

    - hosts: localhost
      roles:
         - digital_ocean_droplet

Simply run `ansible-playbook playbook.yml` to ensure you have required machines on Digital Ocean.
To get an inventory without running any playbooks run `ansible-inventory -i digitalocean.yml --graph --vars`
You can also try connecting to the created machines using Dynamic Inventory `ansible -m ping -i digitalocean.yml all`

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
